var RoboHydra			= require('robohydra'),
	RoboHydraHead		= RoboHydra.heads.RoboHydraHead;

exports.getBodyParts = function(config){
	return {
		heads: [
			new RoboHydraHead({
				name: "Oauth",
				path: "/oauth2/token",
				handler: function(req, res){
					if(req.method == 'POST'){
						var body = JSON.parse(req.rawBody);
						if(body.grant_type == 'password'){
							if(body.username == 'test2' && body.password == 'test2'){
								res.headers['content-type'] = 'application/json';
								res.send('{"access_token":"5980f7466faf4f6794b1ba2c700575e4","expires_in":3600,"scope":{"user":4,"enterprise":null,"meeting":null,"appPermissions":null,"partitionName":"live"}}');
							}
						}
					}
				}
			}),
			new RoboHydraHead({
				name: "User",
				path: "/v1/user/:userId",
				handler: function(req, res){
					if(req.method == "GET"){
						if(req.params.userId == 4){
							res.headers['content-type'] = 'application/json';
							res.send('{"id":4,"username":"test2","firstName":"Javaris","lastName":"Javarison-Lamar","emailId":"ben+test2@bluejeansnet.com","company":"University of Middle Tennessee","middleName":"Jamar","title":"Player from the East","phone":"8459860123","profilePicture":"images/profile_pictures/test2_headlights[1].png","timezone":"US/Pacific","timeFormat":12,"language":"en","skypeId":null,"gtalkId":null,"marketoId":0,"optOutOffers":false,"optOutNews":false,"geoInfo":null,"howDidYouHear":"","linkedinProfileUrl":"","lastLogin":1355096669000,"dateJoined":1348512790000}')
						}
					}
				}
			}),
			new RoboHydraHead({
				name: "PersonalMeetingRoom",
				path: "/v1/user/:userId/room",
				handler: function(req, res){
					if(req.method == "GET"){
						if(req.params.userId == 4){
							res.headers['content-type'] = 'application/json';
							res.send('{"id":3,"name":"test2","numericId":"6669967263","personalMeetingId":2,"moderatorPasscode":"0925","participantPasscode":"","backgroundImage":"images/backgrounds/thumbnails/Scarlet_go_thumbnail.gif","defaultLayout":"2","welcomeMessage":"Welcome to ben.test2\'s meeting room","allow720p":"defaultOptions.allow720p","playAudioAlerts":false,"showVideoAnimations":false,"publishMeeting":false,"encryptionType":"NO_ENCRYPTION","videoBestFit":false,"showAllParticipantsInIcs":false,"isModeratorLess":false,"idleTimeout":0}');
						}
					}
				}
			}),
			new RoboHydraHead({
				name: "PairingCode",
				path: "/v1/user/:userId/live_meetings/:meetingId/pairing_code/SIP",
				handler: function(req, res){
					if(req.method == "POST"){
						res.headers['content-type'] = 'application/json';
						res.send('{"uri":"sip:LYKHNA@10.4.5.235","endpointGuid":"9134aa7e-77a1-4636-a47e-c4396a866e4a","turnservers":[{"protocol":"udp","address":"10.4.5.235","port":5000,"username":"go5iiGaiIeL0quah","password":"rei5Ahqucah3aW6u"},{"protocol":"tcp","address":"10.4.5.235","port":5000,"username":"go5iiGaiIeL0quah","password":"rei5Ahqucah3aW6u"}]}');
					}
				}
			})
		]
	};
};